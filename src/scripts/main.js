console.clear();
//console.inspect(('h1'));
// style console content!!!!
//console.log('%cHello world', 'font-size:40px;color:#fff;text-shadow:0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);');
/*
console.log('bum bác ....');
console.info('bum bác ....');
console.warn('bum bác ....');
console.error('bum bác ....');
*/






var planetsEnumURL = 'http://192.168.0.8:8007/src/data/enums__planets.json';
var zodiacEnumURL = 'http://192.168.0.8:8007/src/data/enums__zodiac.json';




// Get Planets
//======================
var requestPlanets = new XMLHttpRequest();
requestPlanets.open('GET', planetsEnumURL);
requestPlanets.responseType = 'json';
requestPlanets.send();


// Returned data
requestPlanets.onload = function() {
  var planets = requestPlanets.response;  // JSON data as JS object

  // populate it into HTML
  document.querySelector(".planet__name").textContent = planets.sun.name;
  document.querySelector(".planet__symbol").textContent = planets.sun.symbol;
  document.querySelector(".planet__hebrewLetter").textContent = planets.sun.hebrewLetter;
  document.querySelector(".planet__element").textContent = planets.sun.element;
  document.querySelector(".planet__zodiacSignRuler").textContent = planets.sun.zodiacSignRuler;
  document.querySelector(".planet__tarotCard").textContent = planets.sun.tarotCard;
  //document.querySelector(".planet__hebrewLetterName").textContent = planets.sun.hebrewLetterName;


  // list all Hebrew letters
  for (var key in planets) {
    if (planets.hasOwnProperty(key)) {
      //console.log(key + " -> " + planets[key].name);
      var li = document.createElement("li");
          li.innerHTML = planets[key].hebrewLetter + ' ' + planets[key].hebrewLetterName;
      document.querySelector(".hebrewLetters--list").appendChild(li);
    }
  }


  // list all planet symbols
  for (var key in planets) {
    if (planets.hasOwnProperty(key)) {
      //console.log(key + " -> " + planets[key].name);
      var li = document.createElement("li");
          li.innerHTML = planets[key].symbol + ' ' + planets[key].name;
      document.querySelector(".planetSymbols--list").appendChild(li);
    }
  }


  // list all Tarot cards
  for (var key in planets) {
    if (planets.hasOwnProperty(key)) {
      var li = document.createElement("li");
          li.innerHTML = planets[key].symbol + ' ' + planets[key].tarotCard;
      document.querySelector(".tarotCards--list").appendChild(li);
    }
  }


  // list all Elements
  for (var key in planets) {
    if (planets.hasOwnProperty(key)) {
      var li = document.createElement("li");
          li.innerHTML = planets[key].element;
      document.querySelector(".fiveElements--list").appendChild(li);
    }
  }
}



// Get Zodiac
//======================
var requestZodiac = new XMLHttpRequest();
requestZodiac.open('GET', zodiacEnumURL);
requestZodiac.responseType = 'json';
requestZodiac.send();


// Returned data
requestZodiac.onload = function() {
  var zodiac = requestZodiac.response;  // JSON data as JS object


  // populate it into HTML
  document.querySelector(".planet__zodiacSignSymbol").textContent = zodiac.leo.symbol;


  // list all Zodiac signs
  for (var key in zodiac) {
    if (zodiac.hasOwnProperty(key)) {
      //console.log(key + " -> " + zodiac[key].symbol);
      var li = document.createElement("li");
          li.innerHTML = zodiac[key].symbol + ' ' + zodiac[key].name;
      document.querySelector(".zodiac__symbols--list").appendChild(li);
    }
  }
}
